import sys
import os
import numpy as np
import csv




# Select a list of languages and features to use, and outputs the appropriate
# states as a binary table

def main():

    nRows = 15169

    inputdata = [None for y in range(nRows)]

    y = 0
    with open('langdata2.csv', 'rb') as datafile:
        for row in datafile:
            inputdata[y] = row.split('|')
            y = y + 1

#   for i in range(5):
#        print inputdata[i]
    
    data_excl = [[None for x in range(3)] for y in range(nRows)]

    languages = [None for y in range(nRows)]
    features = [None for y in range(nRows)]
    values = [None for y in range(nRows)]

    for i in range(nRows):
        languages[i] = inputdata[i][0]
        features[i] = inputdata[i][1]
        values[i] = inputdata[i][2]


        for j in range(3):
            data_excl[i][j] = inputdata[i][j]


    lang_set = set(languages)
    features_set = set(features)

    languages_unique = sorted(list(lang_set))
    features_unique = sorted(list(features_set))

    print "Unique languages: "
    print languages_unique
    print "\n"

    print "Unique features: "
    print features_unique



    nLangs = len(languages_unique)
    nFeatures = len(features_unique)

    print "Number of languages: ", 
    print nLangs
    print "Number of features: ", 
    print nFeatures


    # Table that indicates whether given language has a value for a given feature at all
    data_indicate_feature = np.zeros( (nLangs, nFeatures),  dtype=np.int32)
    for i in range(nRows):
        lang_id = languages_unique.index( languages[i] )
        feature_id = features_unique.index( features[i] )

        data_indicate_feature[lang_id, feature_id] = 1

    feature_counts = np.sum(data_indicate_feature, axis=0)



    # Select out the features that we want
    # (Here, we choose the features that are common to more than 200 languages)
    candidate_features = np.where( feature_counts >= 200 )

    print "All candidate features: "
    print candidate_features[0]

    ###### Find out which languages have all of the features above

    # Filter out appropriate feature columns
    data_candidates = data_indicate_feature[:, candidate_features[0]]
    print data_candidates.shape

    # Sum row-wise 
    candidate_feature_counts = np.sum( data_candidates, axis=1 )
    print candidate_feature_counts

    # Find all languages that have all these features 
    # -  - if count equals dimension, language has all these features indicated
    allfeature_languages = np.where( candidate_feature_counts == candidate_features[0].shape[0])
    print "All candidate languages: "
    print allfeature_languages[0]


    # Important parameters
    nGoodFeatures = candidate_features[0].shape[0]
    nGoodLangs = allfeature_languages[0].shape[0]


    print "Number of good languages: ",
    print nGoodLangs
    print "Number of good features: ",
    print nGoodFeatures



    # Compile list of feature-complete languages
    # 1 will indicate "Yes", 0 will indicate "No"
    feature_complete_languages = np.zeros( (nGoodLangs, nGoodFeatures), dtype=np.int32 )  # float until further check
    # Test
    feature_complete_languages.fill(-999)




    allfeature_languages_list = list(allfeature_languages[0])
    candidate_features_list = list(candidate_features[0])


    for i in range(nRows):


        lang_id = languages_unique.index( languages[i] )
        feature_id = features_unique.index( features[i] )

        if lang_id in allfeature_languages[0] and feature_id in candidate_features[0]:


            new_lang_id = allfeature_languages_list.index( lang_id )
            new_feature_id = candidate_features_list.index( feature_id )


            s = values[i]
            if s == "Yes":
                feature_complete_languages[new_lang_id, new_feature_id] = 1
            elif s == "No":
                feature_complete_languages[new_lang_id, new_feature_id] = 0
            else:
                sys.exit("Invalid feature value, abort")

    # Save our table
    np.savetxt('results.txt', feature_complete_languages, fmt='%d')

    print np.sum(feature_complete_languages, axis=0).astype(np.float32) / nLangs



    #print out the actual languages we select
    for i in range(allfeature_languages[0].shape[0]):
        print languages_unique[ allfeature_languages[0][i] ]




if __name__ == '__main__':
    main()