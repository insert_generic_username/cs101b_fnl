import sys
import os
import numpy as np
import csv
import sdm
import matplotlib.pyplot as plt

import scipy.stats as ss




# Calculates non-normalized language corruption, as well as
# compares language data to random data for corruption trends

def kanerva(data):
    #data = np.loadtxt('results.txt', dtype=np.int32)
    #data = data[0:128]

    print data.shape
    print data[0:5]

    nLangs = data.shape[0]
    nDims = data.shape[1]

    nHardLocs = 5000

    sdm.set_dimension(nDims)
    sdm.set_radius(nDims/4)

    sdm.set_sample(nHardLocs)

    sdm.initialize()

    bitstrings = [None for x in range(nLangs)]

    for i in range(nLangs):
        bitstrings[i] = sdm.Bitstring(zero=True)

        for j in range(nDims):
            if data[i,j] == 1:
                bitstrings[i].bitset(j)

        sdm.thread_write(bitstrings[i], bitstrings[i])

    corruptions = np.empty( nLangs, dtype=np.int32 )

    for i in range(nLangs):
        b = sdm.thread_read(bitstrings[i])
        corruptions[i] = bitstrings[i].distance_to(b)
        #print corruptions[i]
    print "uncorrupted error"
    print np.mean(corruptions)


    corruption_across_features = np.empty( nDims, dtype=np.float32 )

    #return corruptions
    print "errors with corruption"
    for i in range(nDims):
        corruptions = np.empty( nLangs, dtype=np.int32 )
        for j in range(nLangs):
            corrupted = bitstrings[j].copy()
            corrupted.bitswap(i)
            b = sdm.thread_read(corrupted)
            corruptions[j] = bitstrings[j].distance_to(b)
        corruption_across_features[i] = np.mean(corruptions)
        print np.mean(corruptions)

    #plt.scatter( np.sum(data, axis=0), corruption_across_features )
    #plt.ylim
    #plt.show()

    return corruption_across_features


    

def main():

    data = np.loadtxt('results.txt', dtype=np.int32)

    nLangs = data.shape[0]
    nDims = data.shape[1]

    # GARBAGE DATA
    #data = np.random.randint(2, size=(nLangs, nDims))


    # SWEEP DATA
    data = np.empty( (nLangs, nDims), dtype=np.int32 )
    for i in range(nDims):
        for j in range(nLangs):
            if 8*i < j:
                data[j,i] = 1
            else:
                data[j,i] = 0

    for i in range(nDims):
        np.random.shuffle(data[:,i])


    # COMMENT OUT THIS LINE to test random data
    data = np.loadtxt('results.txt', dtype=np.int32)




    print data[0:30]

    nRuns = 100

    corruptions = np.empty( (nDims, nRuns) )

    for i in range(nRuns):
        corruptions[:,i] = kanerva(data)
        reload(sdm)

    #for i in range(nRuns):
    #    print corruptions[:,i]

    print "\n\nMean corruptions: "
    mean_corruptions = np.mean(corruptions, axis=1)
    print mean_corruptions
    print "Stdev: "
    print np.std(corruptions, axis=1)

    plt.figure(0)
    #plt.bar( np.arange(nDims), mean_corruptions)
    #plt.show()
    plt.scatter( np.sum(data, axis=0)/(float(nLangs)), mean_corruptions )
    plt.xlim([0.0, 1.0])
    #plt.ylim([0.0, 4.0])
    plt.title('Corruption of features relative to feature frequency (Actual data)')
    plt.xlabel('Frequency of feature among languages')
    plt.ylabel('Hamming distance (avg)')
    plt.show()
    #plt.savefig(sys.argv[1])

    print np.sum(data, axis=0)
    print np.sum(data, axis=0).shape


    #print mean_corruptions
    for i in range(nDims):
        print mean_corruptions[i]


def main2():

    corruptions1 = kanerva()
    reload(sdm)
    corruptions2 = kanerva()

    corruptions = np.empty( (corruptions1.shape[0], 2), dtype=np.int32 )
    corruptions[:,0] = corruptions1
    corruptions[:,1] = corruptions2

    print corruptions

    plt.scatter(corruptions1, corruptions2)
    #plt.show()


def main3():
    kanerva()

if __name__ == '__main__':
    main()
