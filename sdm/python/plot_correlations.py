import sys
import os
import numpy as np
import csv
import sdm
import matplotlib.pyplot as plt



def main():

    np.random.seed(42)

    corruptions1 = np.loadtxt('r5.txt')
    corruptions2 = np.loadtxt('r6.txt')

    corruptions = np.empty( (corruptions1.shape[0], 2), dtype=np.int32 )
    corruptions[:,0] = corruptions1
    corruptions[:,1] = corruptions2

    print corruptions

    plt.scatter(corruptions1  + np.random.rand(corruptions1.shape[0]) / 10, corruptions2 + np.random.rand(corruptions1.shape[0]) / 10 )
    plt.show()


if __name__ == '__main__':
    main()