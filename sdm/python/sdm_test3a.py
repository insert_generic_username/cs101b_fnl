import sys
import os
import numpy as np
import csv
import sdm
import matplotlib.pyplot as plt

import scipy.stats as ss


# Calculates normalized language corruption

def kanerva():
    data = np.loadtxt('results.txt', dtype=np.int32)
    #data = data[0:128]

    print data.shape
    print data[0:5]

    nLangs = data.shape[0]
    nDims = data.shape[1]

    nHardLocs = 5000

    reload(sdm)
    sdm.set_dimension(nDims)
    sdm.set_radius(nDims/4)
    sdm.set_sample(nHardLocs)
    sdm.initialize()

    corruptionsPerBit = []

    data_orig = data

    for corruptedBit in range(nDims):


        idxs0 = np.where( data_orig[:,corruptedBit] == 0 )
        idxs1 = np.where( data_orig[:,corruptedBit] == 1 )

        data0 = data_orig[idxs0]
        data1 = data_orig[idxs1]
        if data0.shape[0] == 0 or data1.shape[0] == 0:
            print "skippin"
            continue

        if data0.shape[0] > data1.shape[0]:
            np.random.shuffle(data0)
            data0, data1 = data1, data0
        else:
            np.random.shuffle(data1)

        numdatas = data0.shape[0]
        #print numdatas

        data = np.zeros((2 * numdatas, data0.shape[1]))
        data[0:numdatas,:] = data0
        data[numdatas:numdatas * 2,:] = data1[0:numdatas,:]

        if(data.shape[0] < 95):
            print "skippin"
            corruptionsPerBit.append(0)
            continue

        np.random.shuffle(data)
        data = data[0:95, :]
        nLangs = data.shape[0]

        reload(sdm)
        sdm.set_dimension(nDims)
        sdm.set_radius(nDims/4)
        sdm.set_sample(nHardLocs)
        sdm.initialize()
        bitstrings = [None for x in range(nLangs)]
        for i in range(nLangs):
            bitstrings[i] = sdm.Bitstring(zero=True)
    
            for j in range(nDims):
                if data[i,j] == 1:
                    bitstrings[i].bitset(j)
    
            corrupted = bitstrings[i].copy()
            if i == -1:
                corrupted.bitswap(corruptedBit)
            #sdm.thread_write(bitstrings[i], bitstrings[i])
            sdm.thread_write(corrupted, bitstrings[i])
    
        corruptions = np.empty( nLangs, dtype=np.int32 )
    
        for i in range(nLangs):
            b = sdm.thread_read(bitstrings[i])
            corruptions[i] = bitstrings[i].distance_to(b)
            #print corruptions[i]
    
        print np.mean(corruptions)
        corruptionsPerBit.append(np.mean(corruptions))
    return np.array(corruptionsPerBit)

#    for i in range(nDims):
#        corruptions = np.empty( nLangs, dtype=np.int32 )
#        for j in range(nLangs):
#            b = sdm.thread_read(corrupted)
#            corruptions[j] = bitstrings[j].distance_to(b)
#        print np.mean(corruptions)

    



def main2():

    corruptions1 = kanerva()
    reload(sdm)
    corruptions2 = kanerva()
    print corruptions1
    print corruptions2

    corruptions = np.empty( (corruptions1.shape[0], 2), dtype=np.float32 )
    corruptions[:,0] = corruptions1
    corruptions[:,1] = corruptions2

    print corruptions

    plt.scatter(corruptions1, corruptions2)
    plt.show()

    print "Pearson correlation: "
    print ss.pearsonr(corruptions1, corruptions2)

def main():

    data = np.loadtxt('results.txt', dtype=np.int32)
    nLangs = data.shape[0]
    nDims = data.shape[1]

    nRuns = 100

    corruptions = np.empty( (nDims, nRuns) )

    for i in range(nRuns):
        corruptions[:,i] = kanerva()
        reload(sdm)

    #for i in range(nRuns):
    #    print corruptions[:,i]

    print "\n\nMean corruptions: "
    mean_corruptions = np.mean(corruptions, axis=1)
    print mean_corruptions
    print "Stdev: "
    print np.std(corruptions, axis=1)

    plt.figure(0)
    plt.bar( np.arange(nDims), mean_corruptions)
    #plt.show()

    for i in range(nDims):
        print mean_corruptions[i]


def main3():
    kanerva()

if __name__ == '__main__':
    main()
