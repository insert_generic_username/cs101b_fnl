import sys
import os
import numpy as np
import csv




# Filter out language features that do not have a binary (Yes/No) answer

def yesno(s):
    if s == "Yes" or s == "No":
        return 1
    else:
        return 0



def main():

    # If Yes/No, set as 1, otherwise, set as 0
    data = np.genfromtxt('langdata.csv', delimiter='|', converters={2 : yesno}, usecols=(2,) )

    #print data.shape

    rows_incl = np.where( data == 1 )[0]

    #print rows_incl.shape



    i = 0
    with open('langdata.csv', 'rb') as datafile:
        for row in datafile:
            if i in rows_incl:
                print row,
            i = i + 1


if __name__ == '__main__':
    main()